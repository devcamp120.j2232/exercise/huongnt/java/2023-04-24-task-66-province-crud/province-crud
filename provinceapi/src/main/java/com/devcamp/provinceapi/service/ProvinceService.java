package com.devcamp.provinceapi.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.repository.IProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository pProvinceRepository;

    public ArrayList<CProvince> getAllProvince() {
        ArrayList<CProvince> provinceList = new ArrayList<>();
        pProvinceRepository.findAll().forEach(provinceList::add);
        return provinceList;

    }

    // get district by provinceid
    public Set<CDistrict> getDistrictByProvinceId(int id) {
        CProvince vProvince = pProvinceRepository.findById(id);
        if (vProvince != null) {
            return vProvince.getDistricts();
        } else
            return null;
    }

    // get province by id
    public CProvince getProvinceById(int id) {
        CProvince vProvince = pProvinceRepository.findById(id);
        if (vProvince != null) {
            return vProvince.get();
        } else
            return null;
    }

        // create province
        public CProvince createOrder(CProvince cProvince) {
            try {
                CProvince savedRole = pProvinceRepository.save(cProvince);
                return savedRole;
            } catch (Exception e) {
                return null;
            }
        }

    //update
    public CProvince updateUser(int id, CProvince cProvince) {
		if (pProvinceRepository.findById(id) != null) {
			CProvince newProvince = pProvinceRepository.findById(id).get();
			newProvince.setCode(cProvince.getCode());
			newProvince.setName(cProvince.getName());
			CProvince savedProvince = pProvinceRepository.save(newProvince);
			return savedProvince;
		} else {
			return null;
		}
	}
}
