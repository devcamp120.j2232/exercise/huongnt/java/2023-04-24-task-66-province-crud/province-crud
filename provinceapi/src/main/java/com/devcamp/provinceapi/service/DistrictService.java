package com.devcamp.provinceapi.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CWard;
import com.devcamp.provinceapi.repository.IDistrictRepository;

@Service
public class DistrictService {
    @Autowired
    IDistrictRepository pDistrictRepository;

    public ArrayList<CDistrict> getDistrictList() {
        ArrayList<CDistrict> listDistrict = new ArrayList<>();
        pDistrictRepository.findAll().forEach(listDistrict::add);
        return listDistrict;
    }

    // get ward by districtid
    public Set<CWard> getWardByDistrictId(int id) {
        CDistrict vDistrict = pDistrictRepository.findById(id);
        if (vDistrict != null) {
            return vDistrict.getWards();
        } else
            return null;
    }

}
