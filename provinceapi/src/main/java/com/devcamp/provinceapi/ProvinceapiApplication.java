package com.devcamp.provinceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceapiApplication.class, args);
	}

}
