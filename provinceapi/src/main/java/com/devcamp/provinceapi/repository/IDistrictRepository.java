package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict,Long>{
    CDistrict findById (int id);
    
}
