package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CWard;

public interface IWardRepository extends JpaRepository <CWard, Long> {
    CWard findById(int id);
    
}
