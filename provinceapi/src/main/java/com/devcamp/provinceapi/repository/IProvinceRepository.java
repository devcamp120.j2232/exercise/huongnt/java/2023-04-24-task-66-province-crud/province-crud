package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Long> {
    CProvince findById (int id);
    //CProvince deleteById (int id);
}
