package com.devcamp.provinceapi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CWard;
import com.devcamp.provinceapi.service.DistrictService;


@RestController
@RequestMapping("/")
@CrossOrigin

public class DistrictController {
    @Autowired
    private DistrictService districtService;
    //get all district use service
        @GetMapping("/all-district") // Dùng phương thức GET
        public ResponseEntity<List<CDistrict>> getAllUserApi() {
            try {
                return new ResponseEntity<>(districtService.getDistrictList(), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        //get ward by district_id
        @GetMapping("/ward_by_district")
        public ResponseEntity<Set<CWard>> getWardByDistrictIdApi(@RequestParam(value = "districtId") int id){
            try {
                Set<CWard> districtWard = districtService.getWardByDistrictId(id);
                if (districtWard != null ){
                    return new ResponseEntity<>(districtWard, HttpStatus.OK);
                }
                else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
}
