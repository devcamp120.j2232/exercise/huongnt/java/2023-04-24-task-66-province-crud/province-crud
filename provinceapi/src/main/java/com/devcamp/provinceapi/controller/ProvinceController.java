package com.devcamp.provinceapi.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.repository.IProvinceRepository;
import com.devcamp.provinceapi.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;
    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getAllProvincesApi(){
        try {
            return new ResponseEntity<>(provinceService.getAllProvince(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    //get district by Province_id
    @GetMapping("/district_by_province")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceIdApi(@RequestParam(value = "provinceId") int id){
        try {
            Set<CDistrict> provinceDistrict = provinceService.getDistrictByProvinceId(id);
            if (provinceDistrict != null ){
                return new ResponseEntity<>(provinceDistrict, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //get province by id
        @GetMapping("/province/details/{id}")
        public CProvince getProvinceById(@PathVariable int id) {
            return provinceService.getProvinceById(id);
        }

   // create new user with service
   @PostMapping("/province/create")
   // tạo với service
   public ResponseEntity<Object> createOrder1(@RequestBody CProvince cProvince) {
       try {
           return new ResponseEntity<>(provinceService.createOrder(cProvince), HttpStatus.CREATED);
       } catch (Exception e) {
           System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
           return ResponseEntity.unprocessableEntity()
                   .body("Failed to Create specified province: " + e.getCause().getCause().getMessage());
       }
   }

       // update with service
       @PutMapping("/province/update/{id}")
       public ResponseEntity<Object> updateProvinceById(@PathVariable("id") int id, @RequestBody CProvince cProvince) {
           try {
               return new ResponseEntity<>(provinceService.updateUser(id, cProvince), HttpStatus.OK);
           } catch (Exception e) {
               return new ResponseEntity<>(HttpStatus.NOT_FOUND);
           }
       }


       @Autowired
       IProvinceRepository pIProvinceRepository;
       //delete province by id, xóa luôn 
       @DeleteMapping("/province/delete/{id}")
       public ResponseEntity<Object> deleteProvince(@PathVariable Long id) {
           Optional<CProvince> _userData = pIProvinceRepository.findById(id);
           // nếu khác null
           if (_userData.isPresent()) {
               try {
                   pIProvinceRepository.deleteById(id);
                   return new ResponseEntity<Object>("Deleted id  " +  id ,HttpStatus.OK);
               } catch (Exception e) {
                   // TODO: handle exception
                   return ResponseEntity.unprocessableEntity()
                           .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                   // không thể thực thi hoạt động này
               }
   
           } else {
               return new ResponseEntity<Object>("User does not exist", HttpStatus.INTERNAL_SERVER_ERROR);
           }
       }

       @DeleteMapping("/delete/{id}")
       public ResponseEntity<CProvince> deleteProvinceById(@PathVariable("id") int id) {
         try {
            CProvince province = pIProvinceRepository.findById(id); 
            pIProvinceRepository.delete(province);
           return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
           System.out.println(e);
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
       }
}
