package com.devcamp.provinceapi.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "province")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @OneToMany(targetEntity = CDistrict.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "province_id")
    private Set<CDistrict> districts;
    
    public CProvince() {
    }


    public CProvince(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    

    public void setId(int id) {
        this.id = id;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }


    public String getCode() {
        return code;
    }


    public String getName() {
        return name;
    }


    public Set<CDistrict> getDistricts() {
        return districts;
    }


    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }


    public CProvince get() {
        return this;
    }

    
}
